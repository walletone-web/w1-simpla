=== Wallet One Checkout ===
Contributors: Wallet One Единая касса / Wallet One Checkout
Version: 2.1.0 
Tags: Wallet One, Siimpla, buy now, payment, payment for Simpla
Requires at least: 2.3.7
Tested up to: 2.3.7
Stable tag: 2.3.7
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.en.html

The Wallet One Checkout module is a payment system for Simpla. He it allows to pay for your orders on the site.

== Description ==
If you have an online store on Opencart, then you need a module payments for orders made. This will help you plug the payment system Wallet One Checkout. With our system you will be able to significantly expand the method of receiving payment. This will lead to an increase in the number of customers to your store.

The latest version of the module you can be found at the link https://bitbucket.org/h_elena/w1-simpla/downloads

== Installation ==
1. Register on the site https://www.walletone.com/ru/merchant/
2. Download the module files on the repository and download on the site.
3. Instructions for configuring the module is in site https://www.walletone.com/ru/merchant/modules/simpla-w1/ .

== Screenshots ==

== Changelog ==
= 2.0.0 =
*- Added new universal classes

= 2.1.0 =
*- Change expired date for invoice



== Frequently Asked Questions ==
No recent asked questions 

== Upgrade Notice ==
No recent asked updates

