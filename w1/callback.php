<?php

/**
 * Платежная система Wallet One Единая касса
 * 
 * @cms    Simpla
 * @author     Wallet One
 * @version    2.1.0
 * @license    
 * @copyright  Copyright (c) 2016 Wallet One (http://www.walletone.com)
 * This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

chdir('../../');
require_once('api/Simpla.php');
$simpla = new Simpla();

require_once(dirname(__FILE__).'/resultView.php');
$resView = new W1View();

include_once('walletone/class/client.class.php');
$client = new \WalletOne\client();
$settingsDefault = $client->getSettings();

$order = $simpla->orders->get_order((int) str_replace('_' . $_SERVER['HTTP_HOST'], '', $_POST['WMI_PAYMENT_NO']));

$resView->design->assign('order_id', $order->id);

if (empty($order)) {
  \WalletOne\Helpers::logging(w1ErrorResultOrderOnlyText);
  if (empty($_SERVER['HTTP_USER_AGENT'])) {
    ob_start();
    die('WMI_RESULT=RETRY&WMI_DESCRIPTION=' . $error);
  }
  else {
    $resView->design->assign('error', w1ErrorResultOrderOnlyText);
    echo $resView->fetch();
    die();
  }
}
// Нельзя оплатить уже оплаченный заказ  
if ($order->paid && empty($_SERVER['HTTP_USER_AGENT'])) {
  \WalletOne\Helpers::logging(w1ErrorReturnPaid);
  ob_start();
  die('WMI_RESULT=RETRY&WMI_DESCRIPTION=' . w1ErrorReturnPaid);
}

$payment_method = $simpla->payment->get_payment_method($order->payment_method_id);
$payment_settings = $simpla->payment->get_payment_settings($payment_method->id);
$price = $simpla->money->convert($order->total_price, $payment_method->currency_id, false);

$settings = array(
  'MERCHANT_ID' => $payment_settings['WMI_MERCHANT_ID'],
  'SIGNATURE' => $payment_settings['WMI_SIGNATURE'],
  'CURRENCY_ID' => $payment_settings['WMI_CURRENCY_ID'],
);

$typeUser = (empty($_SERVER['HTTP_USER_AGENT']) ? 'bot' : 'browser');
if ($res = $client->resultValidation($settings, $_POST, $typeUser) !== true) {
  \WalletOne\Helpers::logging($res['error']);
  if (empty($_SERVER['HTTP_USER_AGENT'])) {
    ob_start();
    die('WMI_RESULT=RETRY&WMI_DESCRIPTION=' . $res['error']);
  }
  else {
    $resView->design->assign('error', $res['error']);
    echo $resView->fetch();
    die();
  }
}

$w1Result = $client->getResult($_POST);

//checking on the order amount
if (number_format($price, 2, '.', '') != $w1Result->summ) {
  $error = sprintf(w1ErrorResultOrderSumm, $w1Result->orderId, $w1Result->orderState, $w1Result->orderPaymentId);
  \WalletOne\Helpers::logging($error);
  if (empty($_SERVER['HTTP_USER_AGENT'])) {
    ob_start();
    die('WMI_RESULT=RETRY&WMI_DESCRIPTION=' . $error);
  }
  else {
    $resView->design->assign('error', $error);
    echo $resView->fetch();
    die();
  }
}

// Проверка наличия товара
$purchases = $simpla->orders->get_purchases(array('order_id' => intval($order->id)));
foreach ($purchases as $purchase) {
  $variant = $simpla->variants->get_variant(intval($purchase->variant_id));
  if (empty($variant) || (!$variant->infinity && $variant->stock < $purchase->amount)) {
    die("WMI_RESULT=RETRY&WMI_DESCRIPTION=Нехватка товара $purchase->product_name $purchase->variant_name");
  }
}

switch ($w1Result->orderState) {
  case "accepted":
    // Проверка наличия товара
    $purchases = $simpla->orders->get_purchases(array('order_id' => intval($order_id)));
    foreach ($purchases as $purchase) {
      $variant = $simpla->variants->get_variant(intval($purchase->variant_id));
      if (empty($variant) || (!$variant->infinity && $variant->stock < $purchase->amount)) {
        $error = sprintf(w1ErrorEmptyGoods, $purchase->product_name . ' ' . $purchase->variant_name);
        \WalletOne\Helpers::logging($error);
        if (empty($_SERVER['HTTP_USER_AGENT'])) {
          ob_start();
          die('WMI_RESULT=RETRY&WMI_DESCRIPTION=' . $error);
        }
        else {
          $resView->design->assign('error', $error);
          echo $resView->fetch();
          die();
        }
      }
    }
    
    // Установим статус оплачен
    $simpla->orders->update_order(intval($order->id), array('paid' => 1));

    // Спишем товары
    $simpla->orders->close(intval($order->id));
    $simpla->notify->email_order_user(intval($order->id));
    $simpla->notify->email_order_admin(intval($order->id));

    $text = sprintf(w1OrderResultSuccess, $w1Result->orderId, $w1Result->orderState, $w1Result->orderPaymentId);
    \WalletOne\Helpers::logging($text);
    if (empty($_SERVER['HTTP_USER_AGENT'])) {
      ob_start();
      die('WMI_RESULT=OK');
    }
    else {
      $resView->design->assign('text', $text);
      echo $resView->fetch();
      die();
    }
    break;
  case "created":
    $text = sprintf(w1OrderResultCreated, $w1Result->orderId, $w1Result->orderState, $w1Result->orderPaymentId);
    \WalletOne\Helpers::logging($text);
    return $text;
    break;
  default:
    $text = sprintf(w1OrderResultFail, $w1Result->orderId, $w1Result->orderState, $w1Result->orderPaymentId);
    \WalletOne\Helpers::logging($text);
    return $text;
    break;
}

$resView->design->assign('text', $text);
	
echo $resView->fetch();
