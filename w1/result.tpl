{* Страница результата оплаты *}

{$meta_title = "Ваш заказ № {$order_id}" scope=parent}
<h1>Оплата заказа.</h1>
{if is_array($error)}
    <p style="color: #FF0000;font-size: 120%">
        {foreach from=$myArray item=foo}
            {$error}<br>
        {/foreach}
    </p>
{elseif $error}
    <p style="color: #FF0000;font-size: 120%">{$error}</p>
{/if}

{if $text}
    <p style="color: green;font-size: 120%">{$text}</p>
{/if}