<?php
/**
 * Платежная система Wallet One Единая касса
 * 
 * @cms    Simpla
 * @author     Wallet One
 * @version    2.1.0
 * @license    
 * @copyright  Copyright (c) 2016 Wallet One (http://www.walletone.com)
 * This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

require_once('api/Simpla.php');

class W1 extends Simpla {

  public function checkout_form($order_id, $button_text = null) {
    include_once('walletone/class/client.class.php');
    $client = new \WalletOne\client();
    $settingsDefault = $client->getSettings();
    
    if (empty($button_text)) {
      $button_text = 'Перейти к оплате';
    }

    $order = $this->orders->get_order((int) $order_id);
    $payment_method = $this->payment->get_payment_method($order->payment_method_id);
    $payment_settings = $this->payment->get_payment_settings($payment_method->id);
    $price = $this->money->convert($order->total_price, $payment_method->currency_id, false);

    // Проверка наличия товара
    $purchases = $this->orders->get_purchases(array('order_id' => intval($order_id)));
    foreach ($purchases as $purchase) {
      $variant = $this->variants->get_variant(intval($purchase->variant_id));
      if (empty($variant) || (!$variant->infinity && $variant->stock < $purchase->amount)) {
        $error = sprintf(w1ErrorEmptyGoods, $purchase->product_name . ' ' . $purchase->variant_name);
        \WalletOne\Helpers::logging($error);
        return $error;
      }
    }

    $settings = array(
      'MERCHANT_ID' => $payment_settings['WMI_MERCHANT_ID'],
      'SIGNATURE' => $payment_settings['WMI_SIGNATURE'],
      'CURRENCY_ID' => $payment_settings['WMI_CURRENCY_ID'],
      'order_currency' => 'RUB',
      'order_summ' => number_format($price, 2, '.', ''),
      'order_id' => $order->id,
      'site_name' => $this->settings->site_name,
      'success_url' => $this->config->root_url . '/payment/w1/callback.php',
      'fail_url' => $this->config->root_url . '/order/' . $order->url,
    );

    if (!empty($order->email)) {
      $settings['email'] = $order->email;
    }

    if ($validate = $client->validateParams($settings, '_minishop') !== true) {
      \WalletOne\Helpers::logging($validate['error']);
      echo $validate['error'];
      return;
    }

    $fields = $client->createFieldsForForm($settings);
    $html = $client->createHtmlForm($settings, $fields);
    
    return $html;
  }

}
