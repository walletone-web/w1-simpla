<?php

namespace WalletOne;

class client {
  /**
   *
   * @var string
   */
  public $lang;
  
  function __construct($lang = 'ru') {
    $this->lang = $lang;
  }


  /**
   * Getting html code with list payments with icons
   * 
   * @param string $name
   * @param array $paymentsList
   * @return string
   */
  public function getHtmlPayments($name, $paymentsList = array(), $nameInput = 'w1_', $nameArrayOfInput = '') {
    include_once 'w1.html.class.php';
    include_once 'w1.htmlcms.class.php';
    
    include_once 'w1.settings.class.php';
    include_once 'w1.settingscms.class.php';
    
    $w1Settings = new \WalletOne\W1SettingsCms(array(), $this->lang);
    $w1Html = new \WalletOne\W1HtmlCms($this->lang);
    $html = $w1Html->getHtmlPayments($name, $paymentsList, $w1Settings->filename, $nameInput, $nameArrayOfInput, $name.'_');
    return $html;
  }
  
  /**
   * Creating icon.
   * 
   * @param array $ptenabled
   * @return string
   */
  public function createNewIcon($ptenabled, $ptdisabled = array()){
    include_once 'w1.settings.class.php';
    include_once 'w1.settingscms.class.php';
    
    $w1Settings = new \WalletOne\W1SettingsCms(array(), $this->lang);
    
    include_once 'w1.html.class.php';
    include_once 'w1.htmlcms.class.php';
    
    $w1Html = new \WalletOne\W1HtmlCms();
    $pic = $w1Html->createIcon($ptenabled, $ptdisabled, $w1Settings->filename, $w1Settings->currencyCode);
    return $pic;
  }
  
  /**
   * Getting api url.
   * 
   * @return string
   */
  public function getApiUrl(){
    include_once 'w1.payment.class.php';
    include_once 'w1.paymentcms.class.php';
    
    $w1Payment = new \WalletOne\W1PaymentCms(array(), $this->lang);
    return $w1Payment->paymentUrl;
  }
  
  /**
   * Get default settings from configure file.
   * 
   * @return \WalletOne\W1SettingsCms
   */
  public function getSettings($params = array()){
    include_once 'w1.settings.class.php';
    include_once 'w1.settingscms.class.php';
    
    return new \WalletOne\W1SettingsCms($params, $this->lang);
}
  
  /**
   * Get numder of CMS.
   * 
   * @return \WalletOne\W1SettingsCms
   */
  public function getNumCms($nameCms){
    include_once 'w1.payment.class.php';
    include_once 'w1.paymentcms.class.php';
    $payment = new \WalletOne\W1PaymentCms(array(), $this->lang, $nameCms);
    return $payment->numCms;
  }
  
  public function getPathToDir(){
    $dir = str_replace('\\', '/', __DIR__ . '/../');
    return $dir;
  }

    /**
   * 
   * @param type $filename
   * @return array
   */
  public function getArrayPayments($filename = '') {
    if(empty($filename)){
      include_once 'w1.settings.class.php';
      include_once 'w1.settingscms.class.php';

      $w1Settings = new \WalletOne\W1SettingsCms(array(), $this->lang);
      
      $filename = $w1Settings->filename;
    }
    
    include_once 'w1.html.class.php';
    include_once 'w1.htmlcms.class.php';
    
    $w1Html = new \WalletOne\W1HtmlCms($this->lang);
    
    return $w1Html->getArrayOnlyPayments($filename);
  }
  
  /**
   * Creating array of payments with icons.
   * 
   * @param type $filename
   * @return type
   */
  public function getArrayPaymentWithIcons($filename = '') {
    if(empty($filename)){
      include_once 'w1.settings.class.php';
      include_once 'w1.settingscms.class.php';

      $w1Settings = new \WalletOne\W1SettingsCms(array(), $this->lang);
      
      $filename = $w1Settings->filename;
    }
    
    include_once 'w1.html.class.php';
    include_once 'w1.htmlcms.class.php';
    
    $w1Html = new \WalletOne\W1HtmlCms($this->lang);
    
    return $w1Html->getArrayPayments($filename);
  }
  
  /**
   * 
   * @param type $filename
   * @param type $ptenabled
   * @param type $ptdisabled
   * @return type
   */
  public function getArrayJsonPayments($filename, $ptenabled, $ptdisabled) {
    include_once 'w1.html.class.php';
    include_once 'w1.htmlcms.class.php';
    
    $w1Html = new \WalletOne\W1HtmlCms($this->lang);
    
    return $w1Html->getArrayJsonPayments($filename, $ptenabled, $ptdisabled);
  }
  
  /**
   * Getting languages constants.
   * 
   * @return \WalletOne\W1HtmlCms
   */
  public function getDefaultHtml() {
    include_once 'w1.html.class.php';
    include_once 'w1.htmlcms.class.php';
    return new \WalletOne\W1HtmlCms($this->lang);
  }
  
  /**
   * Getting settings and validate them.
   * 
   * @param array $params
   * @return boolean|string
   */
  public function validateSettings($params){
    include_once 'w1.settings.class.php';
    include_once 'w1.settingscms.class.php';
    
    include_once 'w1.html.class.php';
    include_once 'w1.htmlcms.class.php';
    $w1Html = new \WalletOne\W1HtmlCms($this->lang);
    
    $settings = new \WalletOne\W1SettingsCms($params, $this->lang);
    if (!$settings->required()) {
      return '{"success":"false","data":{"returnCode":"'.  w1ErrorRequired.'"}}';
    }
    if(!$settings->validation()){
      return '{"success":"false","data":{"returnCode":"'. $w1Html->show_messagesOne($settings->errors, $settings->messages).'"}}';
    }
    return true;
  }
  
  /**
   * Validate fields form.
   * 
   * @param array $params
   * @return array|boolean
   */
  public function validateParams($params, $nameCMS) {
    include_once 'w1.settings.class.php';
    include_once 'w1.settingscms.class.php';
    
    include_once 'w1.html.class.php';
    include_once 'w1.htmlcms.class.php';
    
    $w1Html = new \WalletOne\W1HtmlCms($this->lang);
    
    $settings = new \WalletOne\W1SettingsCms($params, $this->lang);
    if (!$settings->required()) {
      return array('error' => $w1Html->show_messages($settings->errors, $settings->messages));
    }
    if(!$settings->validation()){
      return array('error' => $w1Html->show_messages($settings->errors, $settings->messages));
    }
    
    include_once 'w1.invoice.class.php';
    include_once 'w1.invoicecms.class.php';
    
    $w1Invoice = new \WalletOne\W1InvoiceCms($params, $this->lang);
    if (!$w1Invoice->required()) {
      return array('error' => $w1Html->show_messages($w1Invoice->errors, $w1Invoice->messages));
    }
    
    if(!$w1Invoice->validation()){
      return array('error' => $w1Html->show_messages($w1Invoice->errors, $w1Invoice->messages));
    }
    
    include_once 'w1.payment.class.php';
    include_once 'w1.paymentcms.class.php';
    
    //get paymetn fields and validation
    $w1Payment = new \WalletOne\W1PaymentCms($params, $this->lang, $nameCMS);
    if (!$w1Payment->required()) {
      return array('error' => $w1Html->show_messages($w1Payment->errors, $w1Payment->messages));
    }
    if (!$w1Payment->validation()) {
      return array('error' => $w1Html->show_messages($w1Payment->errors, $w1Payment->messages));
    }
    
    return true;
  }
  
  /**
   * Creating fields for form.
   * 
   * @param array $params
   * @return array
   */
  public function createFieldsForForm($params, $nameCms = '_minishop') {
    include_once 'w1.settings.class.php';
    include_once 'w1.settingscms.class.php';
    
    include_once 'w1.html.class.php';
    include_once 'w1.htmlcms.class.php';
    
    $w1Html = new \WalletOne\W1HtmlCms($this->lang);
    
    $settings = new \WalletOne\W1SettingsCms($params, $this->lang);
    
    include_once 'w1.invoice.class.php';
    include_once 'w1.invoicecms.class.php';

    //get data of invoce and validation of fields a invoce
    $w1Invoice = new \WalletOne\W1InvoiceCms($params, $this->lang);
    
    include_once 'w1.payment.class.php';
    include_once 'w1.paymentcms.class.php';
    
    //get paymetn fields and validation
    $w1Payment = new \WalletOne\W1PaymentCms($params, $this->lang, $nameCms);
    
    return $w1Payment->createFormArray($settings, $w1Invoice);
  }
  
  /**
   * Creating html form.
   * 
   * @param array $params
   * @param array $fields
   * @return string
   */
  public function createHtmlForm($params, $fields) {
    include_once 'w1.html.class.php';
    include_once 'w1.htmlcms.class.php';
    
    $w1Html = new \WalletOne\W1HtmlCms($this->lang);
    
    include_once 'w1.payment.class.php';
    include_once 'w1.paymentcms.class.php';
    
    //get paymetn fields and validation
    $w1Payment = new \WalletOne\W1PaymentCms(array(), $this->lang);
    
    $redirect = false;
    if(!empty($params['redirect'])) {
      $redirect = true;
    }
    
    return $w1Html->createForm($fields, $w1Payment->paymentUrl, '', $redirect);
  }
  
  /**
   * The validation of ansew from payment system with partition of type user (bot/browser).
   * 
   * @param type $params
   * @param type $request
   * @return boolean
   */
  public function resultValidation($params, $request, $typeUser = 'universal') {
    include_once 'w1.html.class.php';
    include_once 'w1.htmlcms.class.php';
    $w1Html = new \WalletOne\W1HtmlCms($this->lang);
    include_once 'w1.settings.class.php';
    include_once 'w1.settingscms.class.php';
    $settings = new \WalletOne\W1SettingsCms($params, $this->lang);
    include_once 'w1.result.class.php';
    include_once 'w1.resultcms.class.php';
    
    $w1Result = new \WalletOne\W1ResultCms($request);
    //checking on the empty field 
    if (!$w1Result->required()) {
      if (empty($_SERVER['HTTP_USER_AGENT'])) {
        ob_start();
        die('WMI_RESULT=RETRY&WMI_DESCRIPTION=' . w1ErrorRequired);
      }
      else {
        return array('error' => $w1Html->show_messages($w1Result->errors, $w1Result->messages));
      }
    }
    //checking on the regular expression a field 
    if(!$w1Result->validation()){
      \WalletOne\Helpers::logging('555555');
      if (empty($_SERVER['HTTP_USER_AGENT'])) {
        ob_start();
        die('WMI_RESULT=RETRY&WMI_DESCRIPTION=' . w1ErrorPreg);
      }
      else {
        return array('error' => $w1Html->show_messages($w1Result->errors, $w1Result->messages));
      }
    }
    //check signature
    if($typeUser == 'bot' || $typeUser == 'browser'){
      if(!$w1Result->checkSignatureUniversal($request, $settings->signature, $settings->signatureMethod, $typeUser)){
        if (empty($_SERVER['HTTP_USER_AGENT'])) {
          ob_start();
          die('WMI_RESULT=RETRY&WMI_DESCRIPTION=' . w1ErrorResultSignature);
        }
        else {
          return array('error' => $w1Html->show_messages($w1Result->errors, $w1Result->messages));
        }
      }
    }  
    else{
      if(!$w1Result->checkSignature($request, $settings->signature, $settings->signatureMethod)){
        if (empty($_SERVER['HTTP_USER_AGENT'])) {
          ob_start();
          die('WMI_RESULT=RETRY&WMI_DESCRIPTION=' . w1ErrorResultSignature);
        }
        else {
          return array('error' => $w1Html->show_messages($w1Result->errors, $w1Result->messages));
        }
      }
    }
    
    return true;
  }
  
  /**
   * Get object of result
   * 
   * @param array $request
   * @return \WalletOne\W1ResultCms
   */
  public function getResult($request) {
    include_once 'w1.result.class.php';
    include_once 'w1.resultcms.class.php';
    
    return new \WalletOne\W1ResultCms($request, $this->lang);
  }
  
  /**
   * Getting array of payment for CMS PrestaShop.
   * 
   * @return array
   */
  public function getPaymentArrayPresta(){
    include_once 'w1.settings.class.php';
    include_once 'w1.settingscms.class.php';
    
    $w1Settings = new \WalletOne\W1SettingsCms(array(), $this->lang);
    
    include_once 'w1.html.class.php';
    include_once 'w1.htmlcms.class.php';
    
    $w1Html = new \WalletOne\W1HtmlCms($this->lang);
    
    return $w1Html->getArrayPaymentsPresta($w1Settings->filename);
}
  
  /**
   * The signature generation for the request to the payment system.
   * 
   * @param array $params
   * @param string $sing
   * @param string $method
   * @return string
   */
  public function createSignatureForForm($params, $sing, $method) {
    include_once 'w1.html.class.php';
    include_once 'w1.htmlcms.class.php';
    
    $w1Html = new \WalletOne\W1HtmlCms($this->lang);
    
    include_once 'w1.payment.class.php';
    include_once 'w1.paymentcms.class.php';
    
    //get paymetn fields and validation
    $w1Payment = new \WalletOne\W1PaymentCms(array(), $this->lang);
    
    return $w1Payment->createSignature($params, $sing, $method);
  }
  
}
