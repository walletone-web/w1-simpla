<?php
namespace WalletOne;
/**
 * Abstract class for order data.
 * 
 */
abstract class W1Invoice {
  
  /**
   * Id order in Cms.
   * 
   * @var string 
   */
  public $orderId;
  
  /**
   * Summ of order.
   * 
   * @var string
   */
  public $summ;
  
  /**
   * Default currency.
   * 
   * @var int
   */
  public $currencyId;
  
  /**
   * The buyer first name.
   * 
   * @var string
   */
  public $firstNameBuyer;
  
  /**
   * The buyer last name.
   * 
   * @var string
   */
  public $lastNameBuyer;
  
  /**
   * Email buyer.
   * 
   * @var string
   */
  public $emailBuyer;
  
  /**
   * The set of required invoce fields.
   * 
   * @var array
   */
  public $requiredFields = array();
  
  /**
   * The array for check to the regular expressions.
   * 
   * @var array
   */
  public $fieldsPreg = array();

  /**
   * The array with all errors;
   * 
   * @var array 
   */
  public $errors = array();

  /**
   * To check the required fields.
   * 
   * @return boolean
   */
  abstract public function required();

  /**
   * Checks for required fields and cheks validation these fields.
   * 
   * @param array $param
   * 
   * @return boolean
   */
  abstract public function validation();
  
}
