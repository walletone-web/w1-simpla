<?php
namespace WalletOne;

class W1PaymentCms extends W1Payment  {
  
  /**
   *
   * @var array
   */
  public $fieldsName = array();
  
  public $transaction;

  /**
   * The array with all message;
   * 
   * @var array 
   */
  public $messages = array();
  
  function __construct($params = array(), $lang = 'ru', $nameCms = '') {
    if (!defined('w1PathImg')) {
      define('w1PathImg', '');
    }
    
    $defaultLang = 'ru';
    if($lang != 'ru'){
      $defaultLang = 'en';
    }
    include_once str_replace('\\', '/', __DIR__) . '/../lang/settings.'.$defaultLang.'.php';
    
    include str_replace('\\', '/', __DIR__) . '/../config.php';
    include_once('w1.helpers.php');
    
    $this->paymentUrl = $config['paymentUrl'];
    $this->requiredFields = $config['payment']['requiredFields'];
    $this->fieldsName = $config['payment']['fieldsName'];
    $this->fieldsPreg = $config['payment']['fieldsPreg'];
    $this->numCms = $config['payment']['cms'.$nameCms];
    
    if(!empty($params['site_name'])) {
      $this->siteName = $params['site_name'];
    }
    else{
      $this->siteName = w1SiteName;
    }
    if(!empty($params['success_url'])) {
      $this->successUrl = $params['success_url'];
    }
    if(!empty($params['fail_url'])) {
      $this->failUrl = $params['fail_url'];
    }
    if(!empty($params['transaction'])) {
      $this->transaction = $params['transaction'];
  }
  }
  
  /**
   * Get value of field.
   * 
   * @param string $name
   * @return string
   */
  public function getValue($name){
    $reflect = new \ReflectionClass(get_class($this));
    $property = $reflect->getProperty($name);
    return $property->getValue($this);
  }
  
  /**
   * To check the required fields.
   * 
   * @param array $fields
   * @return boolean
   */
  public function required(){
    $this->errors = array();
    $this->messages = array();
    if(!empty($this->requiredFields)){
      foreach ($this->requiredFields as $value) {
        if(property_exists($this, $value)) {
          $field = $this->getValue($value);
          if(empty($field)){
            $this->errors[] = sprintf(w1ErrorRequired, $this->fieldsName[$value]);
            Helpers::logging(sprintf(w1ErrorRequired, $this->fieldsName[$value]));
          }
        }
        else{
          $this->errors[] = sprintf(w1ErrorRequiredEmpty, $this->fieldsName[$value]);
          Helpers::logging(sprintf(w1ErrorRequiredEmpty, $this->fieldsName[$value]));
        }
      }
      if(!empty($this->errors)){
        $this->messages[] = w1ErrorEmptyFields;
        return false;
      }
    }
    return true;
  }
  
  /**
   * Validation fields.
   * 
   * @return boolean
   */
  public function validation() {
    $this->errors = array();
    $this->messages = array();
    if(!empty($this->fieldsPreg)){
      foreach ($this->fieldsPreg as $key => $value) {
        if(property_exists($this, $key)) {
          $val = $this->getValue($key);
          if (!empty($val) && preg_match($value, $val) == 0) {
            $this->errors[] = sprintf(w1ErrorPreg, $this->fieldsName[$key]);
            Helpers::logging(sprintf(w1ErrorPreg, $this->fieldsName[$key]));
          }
        }
      }
      if(!empty($this->errors)){
        return false;
      }
    }
    return true;
  }
  
  /**
   * Creating an array with the data for payment.
   * 
   * @param array $settings
   *  Settings from module.
   * @param array $invoce
   *  Order data.
   * @return array
   */
  public function createFormArray($settings, $invoce){
    $fields = array();
    $fields['CMS'] = $this->numCms;
    $fields['WMI_CURRENCY_ID'] = $invoce->currencyId;
    $fields['WMI_DESCRIPTION'] = "BASE64:" . base64_encode(sprintf(w1OrderDescr, $invoce->orderId, $this->siteName));
    $fields['WMI_EXPIRED_DATE'] = date('Y-m-d\TH:i:s', time() + 2592000); 
    $fields['WMI_SUCCESS_URL'] = $this->successUrl;
    $fields['WMI_FAIL_URL'] = $this->failUrl;
    $fields['WMI_MERCHANT_ID'] = $settings->merchantId;
    $fields['WMI_PAYMENT_AMOUNT'] = number_format($invoce->summ, 2, '.', '');
    $orderId = $invoce->orderId . (preg_match('/.cms$/ui', $_SERVER['HTTP_HOST']) !== false 
          || preg_match('/.walletone.com$/ui', $_SERVER['HTTP_HOST']) !== false ? '_'.$_SERVER['HTTP_HOST'] : '');
    $fields['WMI_PAYMENT_NO'] = $orderId;
    
    if(!empty($invoce->firstNameBuyer)){
      $fields['WMI_CUSTOMER_FIRSTNAME'] = $invoce->firstNameBuyer;
    }
    if(!empty($invoce->lastNameBuyer)){
      $fields['WMI_CUSTOMER_LASTNAME'] = $invoce->lastNameBuyer;
    }
    if(!empty($invoce->emailBuyer)){
      $fields['WMI_CUSTOMER_EMAIL'] = $invoce->emailBuyer;
      $fields['WMI_RECIPIENT_LOGIN'] = $invoce->emailBuyer;
    }
    if(!empty($settings->paymentSystemEnabled)){
      $fields['WMI_PTENABLED'] = $settings->paymentSystemEnabled;
    }
    if(!empty($settings->paymentSystemDisabled)){
      $fields['WMI_PTDISABLED'] = $settings->paymentSystemDisabled;
    }
    
    if(!empty($this->transaction)){
      $fields['transaction'] = $this->transaction;
    }
    
    //The sorting fields
    foreach ($fields as $name => $val) {
      if (is_array($val)) {
        usort($val, "strcasecmp");
        $fields[$name] = $val;
      }
    }
    uksort($fields, "strcasecmp");
    
    $fields["WMI_SIGNATURE"] = $this->createSignature($fields, $settings->signature, $settings->signatureMethod);
    
    return $fields;
  }
  
  /**
   * Creating an array with the data for payment for bootstrap.
   * 
   * @param array $settings
   *  Settings from module.
   * @param array $invoce
   *  Order data.
   * @return array
   */
  public function createFormBootsrtapArray($settings, $invoce, $paymentUrl){
    $fields = array();
    $fields['CMS'] = array(
      '#type' => 'hidden',
      '#value' => $this->numCms,
    );
    $fields['WMI_CURRENCY_ID'] = array(
      '#type' => 'hidden',
      '#value' => $settings->currencyId,
    );
    if(!empty($invoce->emailBuyer)){
      $fields['WMI_CUSTOMER_EMAIL'] = array(
        '#type' => 'hidden',
        '#value' => $invoce->emailBuyer,
      );
    }
    $fields['WMI_DESCRIPTION'] = array(
      '#type' => 'hidden',
      '#value' => "BASE64:" . base64_encode(sprintf(w1OrderDescr, $invoce->orderId, $this->siteName)),
    );
    
    $fields['WMI_EXPIRED_DATE'] = array(
      '#type' => 'hidden',
      '#value' => date('Y-m-d\TH:i:s', time() + 2592000),
    );
    $fields['WMI_FAIL_URL'] = array(
      '#type' => 'hidden',
      '#value' => $this->successUrl,
    );
    $fields['WMI_MERCHANT_ID'] = array(
      '#type' => 'hidden',
      '#value' => $settings->merchantId,
    );
    $fields['WMI_PAYMENT_AMOUNT'] = array(
      '#type' => 'hidden',
      '#value' => number_format($invoce->summ, 2, '.', ''),
    );
    $orderId = $invoce->orderId . (preg_match('/.cms$/ui', $_SERVER['HTTP_HOST']) !== false 
          || preg_match('/.walletone.com$/ui', $_SERVER['HTTP_HOST']) !== false ? '_'.$_SERVER['HTTP_HOST'] : '');
    $fields['WMI_PAYMENT_NO'] = array(
      '#type' => 'hidden',
      '#value' => $orderId,
    );
    if(!empty($invoce->emailBuyer)){
      $fields['WMI_RECIPIENT_LOGIN'] = array(
        '#type' => 'hidden',
        '#value' => $invoce->emailBuyer,
      );
    }
    $fields['WMI_SUCCESS_URL'] = array(
      '#type' => 'hidden',
      '#value' => $this->failUrl,
    );
    
    $fields['#action'] = $paymentUrl;
    $fields['submit'] = array(
      '#type' => 'submit',
      '#value' => w1OrderSubmitText,
    );
    
    $fields['#after_build'] = array('w1_drupal_commerce_redirect_form_clear');
    
    return $fields;
  }
  
  /**
   * Change array of form for send of payment system.
   * 
   * @param array $form
   * @param string $token
   * @param type $settings
   * @return array
   */
  public function arrayFormation($form, $token, $settings, $redirect = NULL){
    $inputArray = '';
    if(!empty($settings->paymentSystemEnabled)){
      foreach ($settings->paymentSystemEnabled as $key => $value) {
        $inputArray .= '<input type="hidden" name="WMI_PTENABLED" value="' . $value . '">';
      }
    }
    if(!empty($settings->paymentSystemDisabled)){
      foreach ($settings->paymentSystemDisabled as $key => $value) {
        $inputArray .= '<input type="hidden" name="WMI_PTDISABLED" value="' . $value . '">';
      }
    }
    
    $form['form_token']['#value'] = $token;
    $form[$form['submit']['#name']]['#value'] = $form['submit']['#value'];
    $mas = $form['form_id'];
    $form['form_id'] = array(
      '#type' => 'hidden',
      '#value' => 'commerce_checkout_form_payment',
    );
    uksort($form, "strcasecmp");
    
    $fields = array('CMS', 'WMI_CURRENCY_ID', 'WMI_DESCRIPTION', 'WMI_MERCHANT_ID',
      'WMI_FAIL_URL', 'WMI_PAYMENT_AMOUNT', 'WMI_PAYMENT_NO', 'WMI_CUSTOMER_EMAIL',
      'WMI_SUCCESS_URL', 'form_build_id', 'form_token', 'form_id', 'WMI_EXPIRED_DATE',
      'WMI_PTDISABLED', 'WMI_PTENABLED', 'WMI_RECIPIENT_LOGIN',
    );
    
    if(empty($redirect)){
      $fields[] = 'op';
    }

    $field_values = "";
    foreach ($form as $key => $value) {
      if (!in_array($key, $fields)) {
        continue;
      }
      if ($key == 'WMI_PAYMENT_NO') {
        $value['#value'] = mb_convert_encoding($value['#value'], "Windows-1251", "UTF-8");
        $field_values .= $value['#value'];
        if (!empty($settings->paymentSystemDisabled)) {
          foreach ($settings->paymentSystemDisabled as $v) {
            $v = mb_convert_encoding($v, "Windows-1251", "UTF-8");
            $field_values .= $v;
          }
        }
        if (!empty($settings->paymentSystemEnabled)) {
          foreach ($settings->paymentSystemEnabled as $v) {
            $v = mb_convert_encoding($v, "Windows-1251", "UTF-8");
            $field_values .= $v;
          }
        }
      }
      else {
        $value['#value'] = mb_convert_encoding($value['#value'], "Windows-1251", "UTF-8");
        $field_values .= $value['#value'];
      }
    }

    $form['html_text'] = array(
      '#type' => 'item',
      '#markup' => $inputArray,
    );
    
    $method = $settings->signatureMethod;
    $signature = base64_encode(pack("H*", $method($field_values . $settings->signature)));
    $form["WMI_SIGNATURE"] = array(
      '#type' => 'hidden',
      '#value' => $signature,
    );

    unset($form[$form['submit']['#name']]);
    $form['form_id'] = $mas;
    
    uksort($form, "strcasecmp");
    
    if(!empty($redirect) && $redirect == 1){
      $form['js_redirect'] = array(
        '#type' => 'item',
        '#markup' => '<script>document.getElementById("w1-drupal-commerce-redirect-form").submit();</script>',
      );
    }
    
    return $form;
  }

    /**
   * Create signature.
   * 
   * @param array $fields
   * @param string $sig
   * @return string
   */
    public function createSignature($fields, $sig, $method){
    $fieldValues = "";
    foreach ($fields as $value) {
      if (is_array($value)) {
        foreach ($value as $v) {
          $v = iconv("utf-8", "windows-1251", $v);
          $fieldValues .= $v;
        }
      }
      else {
        $value = iconv("utf-8", "windows-1251", $value);
        $fieldValues .= $value;
      }
    }
    
    $signature = base64_encode(pack("H*", $method($fieldValues . $sig)));
    return $signature;
  }
  
}
